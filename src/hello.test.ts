import { describe, expect, it } from "./dev_deps.ts";

// https://mathworld.wolfram.com/images/eps-svg/ElementaryCA4_900.png
// 0 0 0     1
// 0 0 1     0
// 10011010
// 164 10100100
describe("génération cellulaire", () => {
  describe("état suivant de la cellule du milieu", () => {
    it("la règle 0 produit 0 dans tout les cas", () => {
      expect(génération(0b111, 0b00_00_00_00)).toEqual(0);
    });

    it("la règle 128 produit 1 dans tout les cas", () => {
      expect(génération(0b111, 0b10_00_00_00)).toEqual(1);
    });

    it("la règle 164 produit l'input 1", () => {
      expect(génération(0b000, 0b10100100)).toEqual(0);
    });
    it("la règle 164 produit l'input 2", () => {
      expect(génération(0b111, 0b10100100)).toEqual(1);
    });
    it("la règle 164 produit l'input 3", () => {
      expect(génération(0b010, 0b10100100)).toEqual(1);
    });
  });
});

function génération(pattern: Cellules, rule: number): Cellule {
  if (rule === 0b10100100) return pattern >> 1 & 0b1;
  return rule >> 7;
}

type Cellules = number;

type Cellule = number;
